﻿create table ustanova(Id smallint not null, Naziv varchar(30) not null, Lokacija varchar(35) not null, Active bit not null);
create table ucionica (IdUcionice smallint not null, BrUcionice int not null, BrMesta int not null, tipUcionice varchar(30) not null, Active bit not null);
create table users (FirstName varchar(25) not null, LastName varchar(30) not null, Email varchar(30) not null, Username varchar(30) not null, Lozinka varchar(40) not null, TypeOfUser varchar(20) not null);

insert into ustanova values(1, 'Tehnicka skola Becej', 'Uros Predic 1', 1);
insert into ustanova values(2, 'Fakultet tehnickih nauka', 'Dositej Obradovic 6', 1);
insert into ustanova values(3, 'Poljoprivredni fakultet' , 'Dositej Obradovic 8', 1);

insert into ucionica values(1, 101, 30, 'SA_RACUNARIMA', 1);
insert into ucionica values(2, 102, 35, 'BEZ_RACUNARA', 1);
insert into ucionica values(3, 103, 20, 'BEZ_RAZUNARA', 1);

insert into users values('Zika' , 'Zikic' , 'zika12@gmail.com', 'zikacar', 'zigi123', 'Asistent');
insert into users values('Mika' , 'Mikic' , 'mikalegenda@gmail.com', 'mikalegenda', 'mika1234', 'Profesor');
insert into users values('Pera' , 'Peric' , 'pera.peric@gmail.com', 'peraadmin', 'peraperic11', 'Administrator');

alter table ustanova add primary key(Id);
alter table ucionica add primary key(IdUcionice);
alter table users add primary key(Username);
alter table termin add primary key(IdTermin);

create table termin(IdTermin smallint not null, Vreme varchar(20) not null, DaniUNedelji varchar(30) not null, TipNastave varchar(30) not null, Korisnik varchar(30) not null, Active bit not null);

insert into termin values(1, '8a.m do 11a.m', 'ponedeljkom', 'predavanja', 'Mika', 1);
insert into termin values(2, '5p.m do 8p.m', 'utorkom i sredom', 'vezbe', 'Zika', 1);

create table dbo.Administrators (Id smallint not null primary key, constraint fk_users_administrators foreign key (Id) references dbo.users(Id));

create table dbo.Profesor (Id smallint not null primary key, constraint fk_users_profesor foreign key(Id) references dbo.Users(id));

create table dbo.TeacherAssistants(Id smallint not null primary key, Profesor_Id int not null, constraint fk_users_tas foreign key (Id) references dbo.Users(Id), constraint fk_professors_tas foreign key (Profesor_Id) references dbo.Profesor(id))