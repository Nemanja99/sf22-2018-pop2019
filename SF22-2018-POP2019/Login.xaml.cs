﻿using SF22_2018_POP2019.Models;
using SF22_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF22_2018_POP2019
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
        }

        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            Data.ReadUsers();
            Data.ReadUstanove();
            Data.ReadTermine();
            Data.TrazenjeProfesora();
            Data.TrazenjeAsistenata();
            
            String username = txtUsername.Text;
            String password = txtPassword.Password.ToString();
            Data.loginUser = Data.Login(username, password);
            User user = Data.loginUser;

            if (user == null)
            {
                MessageBox.Show($"Wrong username or password!", "Error", MessageBoxButton.OK);
                return;
            }

            switch (user.TypeOfUser.ToString())
            {
                case "ADMINISTRATOR":
                    {
                        AdminWindow admin = new AdminWindow();
                        admin.ShowDialog();
                    }
                    break;
                case "PROFESOR":
                    {
                        //korisnik.Ustanova = Data.DajUstanovuZaKorisnika(korisnik.KorisnickoIme);
                        //Data.UcitavanjeTerminaZaKorisnika(korisnik);
                        TerminWindow terminWindow = new TerminWindow();
                        terminWindow.ShowDialog();
                    }
                    break;
                case "ASISTENT":
                    {
                        //korisnik.Ustanova = Data.DajUstanovuZaKorisnika(korisnik.KorisnickoIme);
                        //Data.UcitavanjeTerminaZaKorisnika(korisnik);
                        TerminWindow terminWindow = new TerminWindow();
                        terminWindow.ShowDialog();
                    }
                    break;
                default:
                    break;
            }
            txtUsername.Text = "";
            txtPassword.Password = "";
        }

        private void BtnGuest_Click(object sender, RoutedEventArgs e)
        {
            Data.ReadUsers();
            Data.ReadUstanove();
            Data.ReadTermine();
            GuestWindow guestWindow = new GuestWindow();
            guestWindow.ShowDialog();
        }
    }
}
