﻿using SF22_2018_POP2019.Models;
using SF22_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SF22_2018_POP2019
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ICollectionView view;
        String selectedFilter = "ACTIVE";
        public MainWindow()
        {
            InitializeComponent();
            InitializeView();
            view.Filter = CustomFilter;
            dgUsers.IsReadOnly = true;
        }

        private bool CustomFilter(object obj)
        {
            User user = obj as User;
            if (selectedFilter.Equals("SEARCH_USERNAME"))
                return user.Active && user.Username.Contains(txtSearchByUsername.Text);
            else if (selectedFilter.Equals("SEARCH_NAME"))
                return user.Active && user.Name.Contains(txtName.Text);
            else if (selectedFilter.Equals("SEARCH_EMAIL"))
                return user.Active && user.Email.Contains(txtEmail.Text);
            else if (selectedFilter.Equals("SEARCH_LASTNAME"))
                return user.Active && user.LastName.Contains(txtRole.Text);
            return user.Active;
        }

        private void InitializeView()
        {
            view = CollectionViewSource.GetDefaultView(Data.Users);
            dgUsers.ItemsSource = view;

            //dgUsers.ItemsSource = Data.Users.FindAll(u => u.Active); sve ono predhodno u reload moye da se napise u jednoj liniji
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Administrator administrator = new Administrator();
            UserWindow userWindow = new UserWindow(administrator);

            if (userWindow.ShowDialog() == true)
            {
                selectedFilter = "ACTIVE";
                view.Refresh();
            }
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            User user = dgUsers.SelectedItem as User;
            if (user == null)
            {
                MessageBox.Show("Warning user not selected", "Warning", MessageBoxButton.OK);
            }
            else
            {
                User oldUser = user.Clone(); // ovo nam treba da u slucaju neko pritisne cancel
                UserWindow userWindow = new UserWindow(user);

                if (!(bool)userWindow.ShowDialog() == true)
                {
                    int indexUser = Data.Users.FindIndex(u => user.Username.Equals(oldUser.Username));
                    Data.Users[indexUser] = oldUser;
                }
                selectedFilter = "ACTIVE";
                view.Refresh();
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            User user = dgUsers.SelectedItem as User;
            if (user == null)
            {
                MessageBox.Show("Warning user not selected", "Warning", MessageBoxButton.OK);
            }
            else
            {
                User user1 = GetUser(user.Username);
                user1.Active = false;

                user1.DeleteUser();

                selectedFilter = "ACTIVE";
                view.Refresh();
            }
        }
        private User GetUser(string username) //User je tip povratne vrednosti ove metode
        {
            foreach (User user in Data.Users)
            {
                if (user.Username.Equals(username))
                {
                    return user;
                }
            }
            return null;
        }

        private void btnSearchUsername_click(object sender, RoutedEventArgs e)
        {
            selectedFilter = "SEARCH_USERNAME";
            view.Refresh();
        }

        private void dgUsers_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Error") || e.PropertyName.Equals("Active") || e.PropertyName.Equals("Termini") || e.PropertyName.Equals("Ustanova") || e.PropertyName.Equals("Id"))
            e.Column.Visibility = Visibility.Collapsed;
        }

        private void BtnSearchName_Click(object sender, RoutedEventArgs e)
        {
            selectedFilter = "SEARCH_NAME";
            view.Refresh();
        }

        private void BtnSearchEmail_Click(object sender, RoutedEventArgs e)
        {
            selectedFilter = "SEARCH_EMAIL";
            view.Refresh();
        }

        private void BtnPocetna_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnSearchRole_Click(object sender, RoutedEventArgs e)
        {
            selectedFilter = "SEARCH_LASTNAME";
            view.Refresh();
        }
    }
}
