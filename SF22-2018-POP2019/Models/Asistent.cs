﻿using SF22_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF22_2018_POP2019.Models
{
    class Asistent : User
    {
        private Profesor _profesor;

        public Profesor Profesor
        {
            get { return _profesor; }
            set { _profesor = value; }
        }

        public Asistent(string name, string lastName , string email, string username, string lozinka , Profesor profesor, bool active) : base(name, lastName, email, username, lozinka , active)
        {
            TypeOfUser = ETypeOfUser.ASISTENT;
            this._profesor = profesor;
        }

        public Asistent() : base()
        {
            TypeOfUser = ETypeOfUser.ASISTENT;
            Profesor profesor = new Profesor();
        }

        public override User Clone()
        {
            Asistent asistent= new Asistent(this.Name, this.LastName, this.Email, this.Username, this.Lozinka , this.Profesor, this.Active);
            
            return asistent;
        }

        public override int SaveUser()
        {
            int id = base.SaveUser();

            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into TeacherAssistants (id, profesor_Id) values(@id, @profesor_id)";
                command.Parameters.Add(new SqlParameter("id", id));

                int profesor_id = Profesor.SelectedUserId();
                command.Parameters.Add(new SqlParameter("professor_id", profesor_id));
                command.ExecuteNonQuery();
            }
            return id;
        }

        public override int SelectedUserId()
        {
            return base.SelectedUserId();
        }

        public override void UpdateUser()
        {
            base.UpdateUser();
        }

        public override void DeleteUser()
        {
            base.DeleteUser();
        }
    }
}
