﻿using SF22_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF22_2018_POP2019.Models
{
    class Administrator : User
    {

        public Administrator(string name, string lastName, string email, string username, string lozinka , bool active) : base(name, lastName, email , username, lozinka , active)
        {
            TypeOfUser = ETypeOfUser.ADMINISTRATOR;
        }

        public Administrator() : base()
        {
            TypeOfUser = ETypeOfUser.ADMINISTRATOR;
        }

        public override User Clone()
        {
            Administrator administrator = new Administrator(this.Name , this.LastName, this.Email, this.Username, this.Lozinka , this.Active);

            return administrator;
        }

        public override int SaveUser()
        {
            int id = base.SaveUser();

            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into Administrators (id) values(@id)";
                command.Parameters.Add(new SqlParameter("id", id));

                command.ExecuteNonQuery(); //necemo imati povratnu vrednost vec samo da ga izbrise
            }
                return id;
        }

        public override int SelectedUserId()
        {
            return base.SelectedUserId();
        }

        public override void UpdateUser()
        {
            base.UpdateUser();
        }

        public override void DeleteUser()
        {
            base.DeleteUser();
        }
    }
}
