﻿using SF22_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF22_2018_POP2019.Models
{
    class Profesor : User
    {
        private ObservableCollection<User> _assistants;

        public ObservableCollection<User> Assistants
        {
            get { return _assistants; }
            set { _assistants = value; }
        }

        public Profesor(string name, string lastName , string email, string username, string lozinka , bool active) : base(name, lastName, email , username, lozinka , active)
        {
            TypeOfUser = ETypeOfUser.PROFESOR;
            this._assistants = new ObservableCollection<User>();
        }

        public Profesor() : base()
        {
            TypeOfUser = ETypeOfUser.PROFESOR;
            this._assistants = new ObservableCollection<User>();
        }

        public override User Clone()
        {
            Profesor profesor = new Profesor(Name, LastName, Email , Username, Lozinka , Active);
            profesor.Assistants = Assistants;
            return profesor;
        }

        public override int SaveUser()
        {
            int id = base.SaveUser();

            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into Profesor (id) values(@id)";
                command.Parameters.Add(new SqlParameter("id", id));

                command.ExecuteNonQuery();
            }

            return id;
        }

        public override int SelectedUserId()
        {
            return base.SelectedUserId();
        }

        public override void UpdateUser()
        {
            base.UpdateUser();
        }

        public override void DeleteUser()
        {
            base.DeleteUser();
        }
    }
}
