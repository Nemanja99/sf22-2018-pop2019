﻿using SF22_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF22_2018_POP2019.Models
{
    public class Termin : INotifyPropertyChanged, IDataErrorInfo
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

        }

        private int _idTermina;

        public int IdTermina
        {
            get { return _idTermina; }
            set { _idTermina = value; OnPropertyChanged("IdTermina"); }
        }

        private string _vremeZauzeca;

        public string VremeZauzeca
        {
            get { return _vremeZauzeca; }
            set { _vremeZauzeca = value; OnPropertyChanged("VremeZauzeca"); }
        }

        private string _daniUNedelji;

        public string DaniUNedelji
        {
            get { return _daniUNedelji; }
            set { _daniUNedelji = value; OnPropertyChanged("DaniUNedelji"); }
        }

        private bool _active;

        public bool Active
        {
            get { return _active; }
            set { _active = value; OnPropertyChanged("Active"); }
        }

        public string Error
        {
            get { return null; }
        }

        private ETypeOfTermini _typeOfTermini;

        public ETypeOfTermini TypeOfTermini
        {
            get { return _typeOfTermini; }
            set { _typeOfTermini = value; OnPropertyChanged("TypeOfTermini"); }
        }

        private User _user;

        public User User
        {
            get { return _user; }
            set { _user = value; OnPropertyChanged("User"); }
        }

        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "IdTermina":
                        if (IdTermina != 0 && IdTermina.Equals(string.Empty))
                            return "field required";
                        break;
                    case "VremeZauzeca":
                        if (VremeZauzeca != null && VremeZauzeca.Equals(string.Empty))
                            return "field required";
                        break;
                    case "DaniUNedelji":
                        if (DaniUNedelji != null && DaniUNedelji.Equals(string.Empty))
                            return "field required";
                        break;
                    case "User":
                        if (User != null && User.Equals(string.Empty))
                            return "field required";
                        break;
                }

                return string.Empty;
            }
        }

        public Termin(int idTermina, string vremeZauzeca, string daniUNedelji, ETypeOfTermini typeOfTermini, User user, bool active)
        {
            this.IdTermina = idTermina;
            this.VremeZauzeca = vremeZauzeca;
            this.DaniUNedelji = daniUNedelji;
            this.TypeOfTermini = typeOfTermini;
            this.Active = active;
            this.User = user;
        }

        public Termin()
        {
            this.IdTermina = 0;
            this.Active = true;
            this.VremeZauzeca = "";
            this.DaniUNedelji = "";
            this.TypeOfTermini = ETypeOfTermini.PREDAVANJA;
            this.User = new Administrator();
        }

        public virtual Termin Clone()
        {
            return null;
        }

        public virtual int SaveTermin()
        {
            int id = -1;
            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into termin ( Vreme, DaniUNedelji, TipNastave, Korisnik, Active) output inserted.idTermina values ( @VremeZauzeca, @DaniUNedelji, @TypeOfTermini, @User, @Active)";

                command.Parameters.Add(new SqlParameter("VremeZauzeca", this.VremeZauzeca));
                command.Parameters.Add(new SqlParameter("DaniUNedelji", this.DaniUNedelji));
                command.Parameters.Add(new SqlParameter("TypeOfTermini", this.TypeOfTermini.ToString()));
                command.Parameters.Add(new SqlParameter("User", this.User));
                command.Parameters.Add(new SqlParameter("Active", this.Active));

                id = (int)command.ExecuteScalar();
            }
            return id;
        }

        public virtual void UpdateTermin()
        {
            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update termin set vremeZauzeca=@vremeZauzeca, daniUNedelji=@daniUNedelji, typeOfTermini=@typeOfTermini, user=@user where idTermina=@idTermina";

                command.Parameters.Add(new SqlParameter("vremeZauzeca", this.VremeZauzeca));
                command.Parameters.Add(new SqlParameter("daniUNedelji", this.DaniUNedelji));
                command.Parameters.Add(new SqlParameter("typeOfTermini", this.TypeOfTermini));
                command.Parameters.Add(new SqlParameter("user", this.User));
                command.Parameters.Add(new SqlParameter("idTermina", this.IdTermina));

                command.ExecuteNonQuery();
            }
        }

        public virtual void DeleteTermin()
        {
            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update dbo.termin set Active = 0 where IdTermin=@IdTermina";

                command.Parameters.Add(new SqlParameter("IdTermina", this.IdTermina));

                command.ExecuteNonQuery();
            }
        }

        public virtual int SelectedTerminId()
        {
            int id = -1;

            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"Select idTermin from termin where idTermin=@IdTermina";
                command.Parameters.Add(new SqlParameter("IdTermin", this.IdTermina));

                id = (int)command.ExecuteScalar();
            }

            return id;
        }

    }
}
