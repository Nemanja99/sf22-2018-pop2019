﻿using SF22_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF22_2018_POP2019.Models
{
    public class Ucionica: INotifyPropertyChanged, IDataErrorInfo
    {

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

        }

        private int _idUcionice;

        public int IdUcionice
        {
            get { return _idUcionice; }
            set { _idUcionice = value; OnPropertyChanged("IdUstanove"); }
        }

        private int _brUcionice;

        public int BrUcionice
        {
            get { return _brUcionice; }
            set { _brUcionice = value; OnPropertyChanged("BrUcionice"); }
        }

        private int _brMesta;

        public int BrMesta
        {
            get { return _brMesta; }
            set { _brMesta = value; OnPropertyChanged("BrMesta"); }
        }

        private bool _active;

        public bool Active
        {
            get { return _active; }
            set { _active = value; OnPropertyChanged("Active"); }
        }

        public string Error
        {
            get { return null; }
        }

        private ETypeOfUcionice _typeOfUcionice;

        public ETypeOfUcionice TypeOfUcionice
        {
            get { return _typeOfUcionice; }
            set { _typeOfUcionice = value; OnPropertyChanged("TypeOfUcionice"); }
        }

        private Ustanova1 _ustanova;

        public Ustanova1 Ustanova
        {
            get { return _ustanova; }
            set { _ustanova = value; OnPropertyChanged("Ustanova"); }
        }

        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "BrUcionice":
                        if (BrUcionice != 0 && BrUcionice.Equals(string.Empty))
                            return "This field is required!";
                        break;
                    case "BrMesta":
                        if (BrMesta != 0 && BrMesta.Equals(string.Empty))
                            return "This field is required!";
                        break;
                }

                return string.Empty;
            }
        }

        public Ucionica(int idUcionice, int brUcionice, int brMesta, ETypeOfUcionice typeOfUcionice, Ustanova1 ustanova, bool active)
        {
            this.IdUcionice = idUcionice;
            this.BrUcionice = brUcionice;
            this.BrMesta = brMesta;
            this.TypeOfUcionice = typeOfUcionice;
            this.Ustanova = ustanova;
            this.Active = active;
        }

        public Ucionica()
        {
            this.IdUcionice = 0;
            this.Active = true;
            this.BrMesta = 0;
            this.BrUcionice = 0;
            this.TypeOfUcionice = ETypeOfUcionice.SA_RACUNARIMA;
            this.Ustanova = new Ustanova1();
        }

        public virtual Ucionica Clone()
        {
            Ucionica ucionica = new Ucionica(IdUcionice, BrUcionice, BrMesta, TypeOfUcionice, Ustanova, Active);
            return ucionica;
        }

        public virtual int SaveUcionica()
        {
            int id = -1;
            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into ucionica (BrUcionice, BrMesta, tipUcionice, Active, IdUstanove) output inserted.idUcionice values (@BrUcionice, @BrMesta, @tipUcionice , @Active, @IdUstanove)";

                //command.Parameters.Add(new SqlParameter("IdUcionice", this.IdUcionice));
                command.Parameters.Add(new SqlParameter("BrUcionice", this.BrUcionice));
                command.Parameters.Add(new SqlParameter("BrMesta", this.BrMesta));
                command.Parameters.Add(new SqlParameter("tipUcionice", this.TypeOfUcionice.ToString()));
                command.Parameters.Add(new SqlParameter("Active", this.Active));
                command.Parameters.Add(new SqlParameter("IdUstanove", this.Ustanova.IdUstanove));

                id = (int)command.ExecuteScalar();
            }
            return id;
        }

        public virtual void UpdateUcionica()
        {
            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update ucionica set brUcionice=@brUcionice, brMesta=@brMesta, tipUcionice=@tipUcionice where idUcionice=@idUcionice";

                command.Parameters.Add(new SqlParameter("brUcionice", this.BrUcionice));
                command.Parameters.Add(new SqlParameter("brMesta", this.BrMesta));
                command.Parameters.Add(new SqlParameter("tipUcionice", this.TypeOfUcionice.ToString()));
                command.Parameters.Add(new SqlParameter("idUcionice", this.IdUcionice));

                command.ExecuteNonQuery();
            }
        }

        public virtual void DeleteUcionica()
        {
            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update ucionica set active=@active where idUcionice=@idUcionice";

                command.Parameters.Add(new SqlParameter("active", this.Active));
                command.Parameters.Add(new SqlParameter("idUcionice", this.IdUcionice));

                command.ExecuteNonQuery();
            }
        }
    }
}
