﻿using SF22_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF22_2018_POP2019.Models
{
    public class Ustanova1 : INotifyPropertyChanged, IDataErrorInfo
    { 

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

        }

        private ObservableCollection<Ucionica> ucionice;

        public ObservableCollection<Ucionica> Ucionice
        {
            get { return ucionice; }
            set { ucionice = value; }
        }

        private int _idUstanove;

        public int IdUstanove
        {
            get { return _idUstanove; }
            set { _idUstanove = value; OnPropertyChanged("IdUstanove"); }
        }

        private string _naziv;

        public string Naziv
        {
            get { return _naziv; }
            set { _naziv = value; OnPropertyChanged("Naziv"); }
        }

        private string _lokacija;

        public string Lokacija
        {
            get { return _lokacija; }
            set { _lokacija = value; OnPropertyChanged("Lokacija"); }
        }

        private bool _active;

        public bool Active
        {
            get { return _active; }
            set { _active = value; OnPropertyChanged("Active"); }
        }

        public string Error
        {
            get { return null; }
        }

        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Naziv":
                        if (Naziv != null && Naziv.Equals(string.Empty))
                            return "This field is required!";
                        break;
                    case "Lokacija":
                        if (Lokacija != null && Lokacija.Equals(string.Empty))
                            return "This field is required!";
                        break;
                }

                return string.Empty;
            }
        }

        public Ustanova1(int idUstanove, string naziv, string lokacija, bool active)
        {
            this._idUstanove = idUstanove;
            this._lokacija = lokacija;
            this._active = active;
            this._naziv = naziv;
            this.Ucionice = new ObservableCollection<Ucionica>();
        }

        public Ustanova1()
        {
            this.IdUstanove = Data.GetMaxId() + 1;
            this._active = true;
            this.Naziv = "";
            this.Lokacija = "";
            this.Ucionice = new ObservableCollection<Ucionica>();
        }

        public virtual Ustanova1 Clone()
        {
            Ustanova1 ustanova = new Ustanova1(IdUstanove, Naziv, Lokacija, Active);
            return ustanova;
        }

        public virtual int SaveUstanova()
        {
            int id = -1;
            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into ustanova (naziv, lokacija, active) output inserted.idUstanove values (@Naziv, @Lokacija, @Active)";

                command.Parameters.Add(new SqlParameter("Naziv", this.Naziv));
                command.Parameters.Add(new SqlParameter("Lokacija", this.Lokacija));
                command.Parameters.Add(new SqlParameter("Active", this.Active));

                id = (int)command.ExecuteScalar();
            }
            return id;
        }

        public virtual void UpdateUstanova()
        {
            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update ustanova set naziv=@naziv, lokacija=@lokacija where idUstanove=@idUstanove";

                command.Parameters.Add(new SqlParameter("naziv", this.Naziv));
                command.Parameters.Add(new SqlParameter("lokacija", this.Lokacija));
                command.Parameters.Add(new SqlParameter("idUstanove", this.IdUstanove));

                command.ExecuteNonQuery();
            }
        }

        public virtual void DeleteUstanova()
        {
            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update ustanova set active=@active where idUstanove=@idUstanove";

                command.Parameters.Add(new SqlParameter("active", this.Active = false));
                command.Parameters.Add(new SqlParameter("idUstanove", this.IdUstanove));

                command.ExecuteNonQuery();
            }
        }
    }
}
