﻿using SF22_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF22_2018_POP2019.Models
{
    public abstract class User: INotifyPropertyChanged, IDataErrorInfo
    {

        private ObservableCollection<Termin> termini;

        public ObservableCollection<Termin> Termini
        {
            get { return termini; }
            set { termini = value; }
        }

        private Ustanova1 ustanova;
        public Ustanova1 Ustanova
        {
            get { return ustanova; }
            set { ustanova = value; }
        }

        private string id;

        public string Id
        {
            get { return id;  }
            set { id = value; OnPropertyChanged("Id"); }
        }

        private string _name;

        public string Name
        {
            get { return _name; }
            set
            { 
               // if(value.Equals(string.Empty))
                   // throw new ArgumentException("This field is required!");
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        private string _lastName;

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; OnPropertyChanged("LastName"); }
        }


        private string _username;

        public string Username
        {
            get { return _username; }
            set { _username = value; OnPropertyChanged("Username"); }
        }

        private string _lozinka;

        public string Lozinka
        {
            get { return _lozinka; }
            set { _lozinka = value; OnPropertyChanged("Lozinka"); }
        }


        private bool _active;

        public bool Active
        {
            get { return _active; }
            set { _active = value; OnPropertyChanged("Active"); }
        }

        private string _email;

        public string Email
        {
            get { return _email; }
            set { _email = value; OnPropertyChanged("Email"); }
        }

        public string Error
        {
            get { return null; }
        }

        public string this[string columnName]
        {
            get {
                    switch (columnName)
                    {
                    case "Name":
                        if (Name != null && Name.Equals(string.Empty))
                            return "field required";
                        break;
                    case "LastName":
                        if (LastName != null && LastName.Equals(string.Empty))
                            return "field required";
                        break;
                    case "Username":
                        if (Username != null &&  Username.Equals(string.Empty))
                            return "field required";
                        break;
                    case "Lozinka":
                        if (Lozinka != null && Lozinka.Equals(string.Empty))
                            return "field required";
                        break;
                    case "Email":
                        if (Email != null &&  Email.Equals(string.Empty))
                            return "field required";
                        break;
                }

                return string.Empty;
                }
        }

        private ETypeOfUser _typeOfUser;

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
           
        }

        public ETypeOfUser TypeOfUser
        {
            get { return _typeOfUser; }
            set { _typeOfUser = value; OnPropertyChanged("TypeOfUser"); }
        }

        public User(string name, string lastName, string email, string username, string lozinka , bool active)//konstruktor sa parametrima
        {
            this.Name = name;
            this.LastName = lastName;
            this.Username = username;
            this.Lozinka = lozinka;
            this.Active = active;
            this.Email = email;
            this.Termini = new ObservableCollection<Termin>();
        }

        public User()
        {
            Name = "";
            LastName = "";
            Email = "";
            Username = "";
            Lozinka = "";
            Active = true;
            this.Termini = new ObservableCollection<Termin>();
        }

        public virtual User Clone()// ovo kreiramo ovako jer je user abstract. Da nije abstract mogli bi i sa metodom Iclone
        {
            return null;
        }

        public virtual int SaveUser()
        {
            int id = -1;
            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into users (Firstname, Lastname , Email, Username, Lozinka , Typeofuser, Active) output inserted.id values (@Name, @LastName , @Email, @Username, @Lozinka , @TypeOfUser, @Active)";

                command.Parameters.Add(new SqlParameter("Name", this.Name));
                command.Parameters.Add(new SqlParameter("LastName", this.LastName));
                command.Parameters.Add(new SqlParameter("Email", this.Email));
                command.Parameters.Add(new SqlParameter("Username", this.Username));
                command.Parameters.Add(new SqlParameter("Lozinka", this.Lozinka));
                command.Parameters.Add(new SqlParameter("TypeOfUser", this.TypeOfUser.ToString()));
                command.Parameters.Add(new SqlParameter("Active", this.Active));

                id = (int)command.ExecuteScalar();
            }
            return id;
        }

        public virtual void UpdateUser()
        {
            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update users set Firstname=@name, Lastname=@lastName , Email=@email, Lozinka=@lozinka where Username=@username";

                command.Parameters.Add(new SqlParameter("name", this.Name));
                command.Parameters.Add(new SqlParameter("lastName", this.LastName));
                command.Parameters.Add(new SqlParameter("email", this.Email));
                command.Parameters.Add(new SqlParameter("lozinka", this.Lozinka));
                command.Parameters.Add(new SqlParameter("username", this.Username));

                command.ExecuteNonQuery();
            }
        }

        public virtual void DeleteUser()
        {
            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update users set Active=@active where Username=@username";

                command.Parameters.Add(new SqlParameter("active", this.Active=false));
                command.Parameters.Add(new SqlParameter("username", this.Username));

                command.ExecuteNonQuery();
            }
        }

        public virtual int SelectedUserId()
        {
            int id = -1;

            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"Select Id from users where Username=@username";
                command.Parameters.Add(new SqlParameter("username", this.Username));

                id = (int)command.ExecuteScalar();
            }

                return id;
        }

    }
}