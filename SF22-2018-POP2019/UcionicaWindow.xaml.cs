﻿using SF22_2018_POP2019.Models;
using SF22_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF22_2018_POP2019
{
    /// <summary>
    /// Interaction logic for UcionicaWindow.xaml
    /// </summary>
    public partial class UcionicaWindow : Window
    {
        ICollectionView view;
        String selectedFilter = "ACTIVE";
        Ustanova1 u;
        public UcionicaWindow(Ustanova1 ustanova)
        {
            InitializeComponent();
            u = ustanova;
            InitializeView();
            view.Filter = CustomFilter;
        }

        private bool CustomFilter(object obj)
        {

            Ucionica ucionica = obj as Ucionica;
           
            if (selectedFilter.Equals("SEARCH_ID"))
                return ucionica.Active && ucionica.IdUcionice.ToString().Contains(txtSifra.Text);
            else if (selectedFilter.Equals("SEARCH_BrUcionice"))
                return ucionica.Active && ucionica.BrUcionice.ToString().Contains(txtBrUcionice.Text);
            else if (selectedFilter.Equals("SEARCH_BrMesta"))
                return ucionica.Active && ucionica.BrMesta.ToString().Contains(txtBrMesta.Text);
            else if (selectedFilter.Equals("SEARCH_TipUcionice"))
                return ucionica.Active && ucionica.TypeOfUcionice.ToString().Contains(txtTipUcionice.Text);
            return ucionica.Active;
            
        }

        private void InitializeView()
        {
            view = CollectionViewSource.GetDefaultView(u.Ucionice);
            dgUcionica.ItemsSource = view;

        }

        private void BtnPocetna_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            Ucionica ucionicaa = new Ucionica();
            ucionicaa.Ustanova = u;
            UcionicaAddEdit addEdit = new UcionicaAddEdit(ucionicaa);


            if (addEdit.ShowDialog() == true)
            {
                selectedFilter = "ACTIVE";
                view.Refresh();
                
            }
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            Ucionica ucionica = dgUcionica.SelectedItem as Ucionica;
            if (ucionica == null)
            {
                MessageBox.Show("Warning the classroom not selected", "Warning", MessageBoxButton.OK);
            }
            else
            {
                Ucionica oldUcionica = ucionica.Clone(); 
                UcionicaAddEdit addEdit = new UcionicaAddEdit(ucionica);

                if (!(bool)addEdit.ShowDialog() == true)
                {
                    int indexUcionica = Data.Ucionice.FindIndex(u => ucionica.IdUcionice.Equals(oldUcionica.IdUcionice));
                    Data.Ucionice[indexUcionica] = oldUcionica;
                }
                selectedFilter = "ACTIVE";
                view.Refresh();
            }
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            Ucionica ucionica = dgUcionica.SelectedItem as Ucionica;
            if (ucionica == null)
            {
                MessageBox.Show("Warning the classroom not selected", "Warning", MessageBoxButton.OK);
            }
            else
            {
                Ucionica ucionica1 = GetUcionica(ucionica.IdUcionice);
                ucionica1.Active = false;

                ucionica1.DeleteUcionica();

                selectedFilter = "ACTIVE";
                view.Refresh();
            }
        }

        private Ucionica GetUcionica(int Id)
        {
            foreach (Ucionica ucionica in Data.Ucionice)
            {
                if (ucionica.IdUcionice.Equals(Id))
                {
                    return ucionica;
                }
            }
            return null;
        }

        private void BtnSearchId_Click(object sender, RoutedEventArgs e)
        {
            selectedFilter = "SEARCH_ID";
            view.Refresh();
        }

        private void BtnSearchBrUcionice_Click(object sender, RoutedEventArgs e)
        {
            selectedFilter = "SEARCH_BrUcionice";
            view.Refresh();
        }

        private void BttSearchBrMesta_Click(object sender, RoutedEventArgs e)
        {
            selectedFilter = "SEARCH_BrMesta";
            view.Refresh();
        }

        private void BtnSearchTipUcionice_Click(object sender, RoutedEventArgs e)
        {
            selectedFilter = "SEARCH_TipUcionice";
            view.Refresh();
        }

        private void DgUcionica_AutoGeneratingColumn_1(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Error") || e.PropertyName.Equals("Active") || e.PropertyName.Equals("Ustanova"))
                e.Column.Visibility = Visibility.Collapsed;
        }
    }
}
