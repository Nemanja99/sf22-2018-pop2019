﻿using SF22_2018_POP2019.Models;
using SF22_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF22_2018_POP2019
{
    /// <summary>
    /// Interaction logic for UstanovaWindow.xaml
    /// </summary>
    public partial class UstanovaWindow : Window
    {
        ICollectionView view;
        String selectedFilter = "ACTIVE";
        public UstanovaWindow()
        {
            InitializeComponent();
            InitializeView();
            view.Filter = CustomFilter;

            if (Data.loginUser.Username.Equals(""))
            {
                btnAdd.Visibility = Visibility.Hidden;
                btnDelete.Visibility = Visibility.Hidden;
                btnEdit.Visibility = Visibility.Hidden;
                btnUcionice.Visibility = Visibility.Hidden;
            }
        }
        private bool CustomFilter(object obj)
        {

            Ustanova1 ustanova = obj as Ustanova1;

            if (selectedFilter.Equals("SEARCH_ID"))
                return ustanova.Active && ustanova.IdUstanove.ToString().Contains(txtId.Text);
            else if (selectedFilter.Equals("SEARCH_Naziv"))
                return ustanova.Active && ustanova.Naziv.ToString().Contains(txtNaziv.Text);
            else if (selectedFilter.Equals("SEARCH_Lokacija"))
                return ustanova.Active && ustanova.Lokacija.ToString().Contains(txtLokacija.Text);
            return ustanova.Active;

        }

        private void InitializeView()
        {
            view = CollectionViewSource.GetDefaultView(Data.Ustanove);
            dgUstanova.ItemsSource = view;

        }

        private void BtnPocetna_Click(object sender, RoutedEventArgs e)
        {
            Login login = new Login();
            login.ShowDialog();
        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            Ustanova1 ustanova = new Ustanova1();
            UstanovaAddEdit addEdit = new UstanovaAddEdit(ustanova);

            if (addEdit.ShowDialog() == true)
            {
                selectedFilter = "ACTIVE";
                view.Refresh();
            }
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            Ustanova1 ustanova = dgUstanova.SelectedItem as Ustanova1;
            if (ustanova == null)
            {
                MessageBox.Show("Warning institution not selected", "Warning", MessageBoxButton.OK);
            }
            else
            {
                Ustanova1 oldUstanova = ustanova.Clone();
                UstanovaAddEdit addEdit = new UstanovaAddEdit(ustanova);

                if (!(bool)addEdit.ShowDialog() == true)
                {
                    int indexUstanova = Data.Ustanove.FindIndex(u => ustanova.IdUstanove.Equals(oldUstanova.IdUstanove));
                    Data.Ustanove[indexUstanova] = oldUstanova;
                }
                selectedFilter = "ACTIVE";
                view.Refresh();
            }
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            Ustanova1 ustanova = dgUstanova.SelectedItem as Ustanova1;
            if (ustanova == null)
            {
                MessageBox.Show("Warning institution not selected", "Warning", MessageBoxButton.OK);
            }
            else
            {
                foreach (Ucionica u in ustanova.Ucionice)
                {
                    u.DeleteUcionica();
                }
                Data.Ustanove.Remove(ustanova);

                ustanova.DeleteUstanova();

                selectedFilter = "ACTIVE";
                view.Refresh();
            }
        }

        /*private Ustanova1 GetUstanova(int Id)
        {
            foreach (Ustanova1 ustanova in Data.Ustanove)
            {
                if (ustanova.IdUstanove.Equals(Id))
                {
                    return ustanova;
                }
            }
            return null;
        }*/

        private void BtnPrikaziId_Click(object sender, RoutedEventArgs e)
        {
            selectedFilter = "SEARCH_ID";
            view.Refresh();
        }

        private void BtnPrikaziNaziv_Click(object sender, RoutedEventArgs e)
        {
            selectedFilter = "SEARCH_Naziv";
            view.Refresh();
        }

        private void BtnPrikaziLokaciju_Click(object sender, RoutedEventArgs e)
        {
            selectedFilter = "SEARCH_Lokacija";
            view.Refresh();
        }

        private void BtnIzlaz_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnUcionice_Click(object sender, RoutedEventArgs e)
        {
            Ustanova1 ustanova = dgUstanova.SelectedItem as Ustanova1;
            if (ustanova == null)
            {
                MessageBox.Show("Ustanova nije selektovana", "Warning", MessageBoxButton.OK);
            }
            else
            {
                UcionicaWindow ucioniceWindow = new UcionicaWindow(ustanova);
                if (ucioniceWindow.ShowDialog() == true)
                {
                    selectedFilter = "Active";
                    view.Refresh();
                }
            }
        }

        private void DgUstanova_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Error") || e.PropertyName.Equals("Active") || e.PropertyName.Equals("Ucionice"))
                e.Column.Visibility = Visibility.Collapsed;
        }
    }
}
