﻿using SF22_2018_POP2019.Models;
using SF22_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF22_2018_POP2019
{
    /// <summary>
    /// Interaction logic for GuestWindow.xaml
    /// </summary>
    public partial class GuestWindow : Window
    {
        public GuestWindow()
        {
            InitializeComponent();
            Administrator administrator = new Administrator();
            Data.loginUser = administrator;
        }

        private void BtnPocetna_Click(object sender, RoutedEventArgs e)
        {
            Login login = new Login();
            login.ShowDialog();
        }

        private void BtnIzlaz_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            MessageBox.Show("Dovidjenja");
        }

        private void BtnUstanove_Click(object sender, RoutedEventArgs e)
        {
            UstanovaWindow ustanova = new UstanovaWindow();
            ustanova.ShowDialog();
        }

        private void BtnZaposleni_Click(object sender, RoutedEventArgs e)
        {
            ZaposleniWindow zaposleni = new ZaposleniWindow();
            zaposleni.ShowDialog();
        }
    }
}
