﻿using SF22_2018_POP2019.Models;
using SF22_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF22_2018_POP2019
{
    /// <summary>
    /// Interaction logic for UserWindow.xaml
    /// </summary>
    public partial class UserWindow : Window
    {
        private enum Status { ADD, EDIT }

        private Status _status;
        private User selectedUser;
        public UserWindow(User user)
        {
            InitializeComponent();
            cmbTypeOfUser.ItemsSource = new List<ETypeOfUser>() { ETypeOfUser.ADMINISTRATOR, ETypeOfUser.PROFESOR, ETypeOfUser.ASISTENT };

            if(user.Username.Equals(""))
            {
                _status = Status.ADD;
            }
            else
            {
                _status = Status.EDIT;
            }
            selectedUser = user;
            this.DataContext = user;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if(_status.Equals(Status.ADD))
            {
                if(ProveraUser(txtUsername.Text))
                {
                    MessageBox.Show($"User with username {txtUsername.Text}" +
                        $"already exists!" , "Error", MessageBoxButton.OK);
                    return;
                }
                else
                {
                    Data.Users.Add(selectedUser);
                    selectedUser.SaveUser();
                }
            }
            if (_status.Equals(Status.EDIT))
                selectedUser.UpdateUser(); 

            this.DialogResult = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {

            this.DialogResult = false;
            this.Close();
        }

        private bool ProveraUser(string username)
        {
            foreach(User user in Data.Users)
            {
                if(user.Username.Equals(username))
                {
                    return true;
                }
            }
            return false;
        }

        private User GetUser(string username) //User je tip povratne vrednosti ove metode
        {
            foreach (User user in Data.Users)
            {
                if (user.Username.Equals(username))
                {
                    return user;
                }
            }
            return null;
        }
    }
}
