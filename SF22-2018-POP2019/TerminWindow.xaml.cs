﻿using SF22_2018_POP2019.Models;
using SF22_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF22_2018_POP2019
{
    /// <summary>
    /// Interaction logic for TerminWindow.xaml
    /// </summary>
    public partial class TerminWindow : Window
    {
        ICollectionView view;
        String selectedFilter = "ACTIVE";
        public TerminWindow()
        {
            InitializeComponent();
            InitializeView();
            view.Filter = CustomFilter;
        }

        private bool CustomFilter(object obj)
        {

            Termin termin = obj as Termin;

            if (selectedFilter.Equals("SEARCH_ID"))
                return termin.Active && termin.IdTermina.ToString().Contains(txtSifra.Text);
            else if (selectedFilter.Equals("SEARCH_VremeZauzeca"))
                return termin.Active && termin.VremeZauzeca.Contains(txtVreme.Text);
            else if (selectedFilter.Equals("SEARCH_DaniUNedelji"))
                return termin.Active && termin.DaniUNedelji.Contains(txtDani.Text);
            else if (selectedFilter.Equals("SEARCH_TipTermina"))
                return termin.Active && termin.TypeOfTermini.ToString().Contains(txtTipTermina.Text);
            return termin.Active;

        }

        private void InitializeView()
        {
            view = CollectionViewSource.GetDefaultView(Data.Termini);
            dgTermin.ItemsSource = view;
        }

        private void BtnPocetna_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            Termin termin = new Termin();
            TerminAddEdit addEdit = new TerminAddEdit(termin);

            if (addEdit.ShowDialog() == true)
            {
                selectedFilter = "ACTIVE";
                view.Refresh();
            }
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            Termin termin = dgTermin.SelectedItem as Termin;
            if (termin == null)
            {
                MessageBox.Show("Warning term not selected", "Warning", MessageBoxButton.OK);
            }
            else
            {
                Termin oldTermin = termin.Clone();
                TerminAddEdit addEdit = new TerminAddEdit(termin);

                if (!(bool)addEdit.ShowDialog() == true)
                {
                    int indexTermin = Data.Termini.FindIndex(u => termin.IdTermina.Equals(oldTermin.IdTermina));
                    Data.Termini[indexTermin] = oldTermin;
                }
                selectedFilter = "ACTIVE";
                view.Refresh();
            }
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            Termin termin = dgTermin.SelectedItem as Termin;
            if (termin == null)
            {
                MessageBox.Show("Warning term not selected", "Warning", MessageBoxButton.OK);
            }
            else
            {
                Console.WriteLine(termin.IdTermina);
                Console.WriteLine("-----------------");
                Termin termin1 = GetTermin(termin.IdTermina);
                termin1.Active = false;

                termin1.DeleteTermin();

                selectedFilter = "ACTIVE";
                view.Refresh();
            }
        }

        private Termin GetTermin(int Id) 
        {
            foreach (Termin termin in Data.Termini)
            {
                if (termin.IdTermina.Equals(Id))
                {
                    return termin;
                }
            }
            return null;
        }

        private void DgTermin_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Error") || e.PropertyName.Equals("Active") || e.PropertyName.Equals("User"))
                e.Column.Visibility = Visibility.Collapsed;
        }

        private void BtnSearchId_Click(object sender, RoutedEventArgs e)
        {
            selectedFilter = "SEARCH_ID";
            view.Refresh();
        }

        private void BtnSearchVreme_Click(object sender, RoutedEventArgs e)
        {
            selectedFilter = "SEARCH_VremeZauzeca";
            view.Refresh();
        }

        private void BtnSearchDani_Click(object sender, RoutedEventArgs e)
        {
            selectedFilter = "SEARCH_DaniUNedelji";
            view.Refresh();
        }

        private void BtnSearchTip_Click(object sender, RoutedEventArgs e)
        {
            selectedFilter = "SEARCH_TipTermina";
            view.Refresh();
        }
    }
}
