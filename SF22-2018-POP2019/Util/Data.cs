﻿using SF22_2018_POP2019.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF22_2018_POP2019.Util
{
    class Data
    {
        public static String Connection_string = @"Data Source=(localdb)\v11.0;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";


        public static List<User> Users { get; set; }
        public static List<Ustanova1> Ustanove { get; set; }
        public static List<Ucionica> Ucionice { get; set; }
        public static List<Termin> Termini { get; set; }
        public static List<Profesor> Profesori { get; set; }
        public static List<Asistent> Asistenti { get; set; }
        public static User loginUser;

        public static void ReadUsers()
        {
            Users = new List<User>();
            using (SqlConnection conn = new SqlConnection(Connection_string))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from users where Active = 1";

                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "users");
                foreach (DataRow row in ds.Tables["users"].Rows)
                {
                    int id = (int)row["Id"];
                    string firstName = (string)row["FirstName"];
                    string lastName = (string)row["LastName"];
                    string email = (string)row["Email"];
                    string username = (string)row["Username"];
                    string lozinka = (string)row["Lozinka"];
                    bool active = (bool)row["Active"];

                    if (row["TypeOfUser"].Equals(ETypeOfUser.ADMINISTRATOR.ToString()))
                    {
                        Administrator administrator = new Administrator(firstName, lastName, email, username, lozinka, active);
                        Users.Add(administrator);
                    }
                    if (row["TypeOfUser"].Equals(ETypeOfUser.PROFESOR.ToString()))
                    {
                        Profesor profesor = new Profesor(firstName, lastName, email, username, lozinka, active);
                        //profesor.Ustanova = DajUstanovuZaKorisnika(profesor.KorisnickoIme);
                        Users.Add(profesor);
                    }
                    if (row["TypeOfUser"].Equals(ETypeOfUser.ASISTENT.ToString()))
                    {
                        //string profesorKorisnicko = NadjiProf(korisnickoIme);
                        Profesor profesor = new Profesor();
                        Asistent asistent = new Asistent(firstName, lastName, email, username, lozinka, profesor, active);
                        //asistent.Ustanova = DajUstanovuZaKorisnika(asistent.KorisnickoIme);
                        Users.Add(asistent);
                    }

                }
            }
        }

        public static void ReadUstanove()
        {
            Ustanove = new List<Ustanova1>();
            using (SqlConnection conn = new SqlConnection(Connection_string))
            {
                conn.Open();

                DataSet data = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from ustanova";

                adapter.SelectCommand = command;
                adapter.Fill(data, "ustanova");

                foreach (DataRow row in data.Tables["ustanova"].Rows)
                {
                    int idUstanove = (int)row["IdUstanove"];
                    string Naziv = (string)row["Naziv"];
                    string Lokacija = (string)row["Lokacija"];
                    bool Active = (bool)row["Active"];

                    Ustanova1 ustanova = new Ustanova1(idUstanove, Naziv, Lokacija, Active);
                    Ustanove.Add(ustanova);
                }
            }
        }

        public static void ReadUcionice(Ustanova1 ustanova)
        {
            ustanova.Ucionice.Clear();
            using (SqlConnection conn = new SqlConnection(Connection_string))
            {
                conn.Open();

                DataSet data = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from ucionica where IdUstanove=@idUstanove";
                command.Parameters.Add(new SqlParameter("idUstanove", ustanova.IdUstanove));

                adapter.SelectCommand = command;
                adapter.Fill(data, "ucionica");

                foreach (DataRow row in data.Tables["ucionica"].Rows)
                {
                    int idUcionice = (int)row["IdUcionice"];
                    int brUcionice = (int)row["BrUcionice"];
                    int brMesta = (int)row["BrMesta"];
                    ETypeOfUcionice tipUcionice = (ETypeOfUcionice)row["tipUcionice"];
                    bool active = (bool)row["Active"];

                    Ucionica ucionica = new Ucionica(idUcionice, brUcionice, brMesta, tipUcionice, ustanova, active);
                    ustanova.Ucionice.Add(ucionica);
                }

            }
        }

        public static void ReadTermine()
        {
            Termini = new List<Termin>();
            using (SqlConnection conn = new SqlConnection(Connection_string))
            {
                conn.Open();

                DataSet data = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from termin";

                adapter.SelectCommand = command;
                adapter.Fill(data, "termin");

                foreach (DataRow row in data.Tables["termin"].Rows)
                {
                    int idTermina = (int)row["IdTermin"];
                    string vreme = (string)row["Vreme"];
                    string daniUNedelji = (string)row["DaniUNedelji"];
                    string tip = (string)row["TipNastave"];
                    ETypeOfTermini tipTermina = ETypeOfTermini.PREDAVANJA;
                    if (tip.Equals("VEZBE"))
                    {
                        tipTermina = ETypeOfTermini.VEZBE;
                    }
                    string username = (string)row["Korisnik"];
                    User korisnik = dajKorisnika(username);
                    bool active = (bool)row["Active"];

                    Termin termin = new Termin(idTermina, vreme, daniUNedelji, tipTermina, korisnik, active);
                    Termini.Add(termin);
                }
            }
        }

        public static User dajKorisnika(String username)
        {
            foreach(User u in Users)
            {
                if(u.Username.Equals(username))
                {
                    return u;
                }
            }
            return null;
        }

        public static User Login(String username, String password)
        {
            foreach (User u in Users)
            {
                if (u.Username.Equals(username) && u.Lozinka.Equals(password) && u.Active == true)
                    return u;
            }
            return null;
        }

        public static void TrazenjeProfesora()
        {
            Profesori = new List<Profesor>();
            foreach (User u in Users)
            {
                if (u.TypeOfUser.ToString().Equals("PROFESOR"))
                {
                    Profesor profesor = new Profesor(u.Name, u.LastName, u.Username, u.Lozinka, u.Email, u.Active);
                    //profesor.Ustanova = k.Ustanova;
                    Profesori.Add(profesor);
                }
            }
        }

        public static void TrazenjeAsistenata()
        {
            Asistenti = new List<Asistent>();
            foreach (User u in Users)
            {
                if (u.TypeOfUser.ToString().Equals("ASISTENT"))
                {
                    Profesor profesor = new Profesor();
                    Asistent asistent = new Asistent(u.Name, u.LastName, u.Email, u.Username, u.Lozinka, profesor, u.Active);
                    //asistent.Ustanova = k.Ustanova;
                    if (profesor != null)
                    {
                        profesor.Assistants.Add(asistent);
                    }
                    else
                    {
                        Asistenti.Add(asistent);
                    }
                }
            }
        }

        public static int GetMaxId()
        {
            int max = 0;

            foreach(Ustanova1 u in Ustanove)
            {
                if(u.IdUstanove > max)
                {
                    max = u.IdUstanove;
                }
            }
            return max;
        }

        public static Ustanova1 DajUstanovuPoNazivu(String s)
        {
            foreach (Ustanova1 ustanova in Ustanove)
            {
                if (ustanova.Naziv.Equals(s) && ustanova.Active == true)
                {
                    return ustanova;
                }
            }
            return null;
        }

        public static Ustanova1 GetUstanovaNazivEdit(String s, Ustanova1 u)
        {
            foreach (Ustanova1 ustanova in Ustanove)
            {
                if (ustanova.Naziv.Equals(s) && ustanova.Active == true && u.IdUstanove != ustanova.IdUstanove)
                {
                    return ustanova;
                }
            }
            return null;
        }

        public static Ucionica DajUcionicuPoBrMesta(string s)
        {
            foreach (Ucionica ucionica in Ucionice)
            {
                if (ucionica.BrMesta.ToString().Equals(s) && ucionica.Active == true)
                {
                    return ucionica;
                }
            }
            return null;
        }

        public static Ucionica GetUcionicaBrMestaEdit(string s, Ucionica u)
        {
            foreach (Ucionica ucionica in Ucionice)
            {
                if (ucionica.BrMesta.ToString().Equals(s) && ucionica.Active == true && u.IdUcionice != ucionica.IdUcionice)
                {
                    return ucionica;
                }
            }
            return null;
        }
    }
}
