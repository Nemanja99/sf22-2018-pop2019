﻿using SF22_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace SF22_2018_POP2019
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            Data.ReadUstanove();
            Data.ReadUsers();
            Data.ReadTermine();

            Data.Ucionice = new List<Models.Ucionica>();

            Login login = new Login();
            login.ShowDialog();
        }
    }
}
