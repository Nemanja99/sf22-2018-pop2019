﻿using SF22_2018_POP2019.Models;
using SF22_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF22_2018_POP2019
{
    /// <summary>
    /// Interaction logic for TerminAddEdit.xaml
    /// </summary>
    public partial class TerminAddEdit : Window
    {
        private enum Status { ADD, EDIT }

        private Status _status;
        private Termin selectedTermin;
        public TerminAddEdit(Termin termin)
        {
            InitializeComponent();
            cmbTipTermina.ItemsSource = new List<ETypeOfTermini>() { ETypeOfTermini.PREDAVANJA, ETypeOfTermini.VEZBE };

           
            if (termin.IdTermina.Equals(0))
            {
                _status = Status.ADD;
            }
            else
            {
                _status = Status.EDIT;
            }
            selectedTermin = termin;
            this.DataContext = termin;
        }

        private void BtnOK_Click(object sender, RoutedEventArgs e)
        {
            if (_status.Equals(Status.ADD))
            {
                if (ProveraTermina(Convert.ToInt32(txtId.Text)))
                {
                    MessageBox.Show($"The term with id {txtId.Text}" +
                        $"already exists!", "Error", MessageBoxButton.OK);
                    return;
                }
                else
                {
                    Data.Termini.Add(selectedTermin);
                    selectedTermin.SaveTermin();
                }
            }
            if (_status.Equals(Status.EDIT))
                selectedTermin.UpdateTermin();

            this.DialogResult = true;
            this.Close();
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private bool ProveraTermina(int id)
        {
            foreach (Termin termin in Data.Termini)
            {
                if (termin.IdTermina.Equals(id))
                {
                    return true;
                }
            }
            return false;
        }

        private Termin GetTermin(int id)
        {
            foreach (Termin termin in Data.Termini)
            {
                if (termin.IdTermina.Equals(id))
                {
                    return termin;
                }
            }
            return null;
        }
    }
}