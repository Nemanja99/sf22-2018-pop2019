﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SF22_2018_POP2019.Validations
{
    class EmailValidationRule : ValidationRule
    {
        Regex regex = new Regex(@"\b[A-Z0-9._]+@[A-Z0-9]{1,10}\.[A-Z]{2,3}\b", RegexOptions.IgnoreCase);
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            /* string v = value as string;
             if (v != null && !v.Equals(string.Empty) && regex.Match(v).Success)
                 return new ValidationResult(true, null);
             if(v.Equals(string.Empty))
                 return new ValidationResult(false, "This field is required");
             return new ValidationResult(false, "Frong format!");*/

            string v = value as string;
            if (v == null || v.Equals(string.Empty))
                return new ValidationResult(false, "This field is required!");
            else if (regex.Match(v).Success)
                return new ValidationResult(true, null);
            return new ValidationResult(false, "Format is wrong!");
        }
    }
}
