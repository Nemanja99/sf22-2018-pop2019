﻿using SF22_2018_POP2019.Models;
using SF22_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF22_2018_POP2019
{
    /// <summary>
    /// Interaction logic for UcionicaAddEdit.xaml
    /// </summary>
    public partial class UcionicaAddEdit : Window
    {
        private enum Status { ADD, EDIT }

        private Status _status;
        private Ucionica selectedUcionica;
        public UcionicaAddEdit(Ucionica ucionica)
        {
            InitializeComponent();
            cmbType.ItemsSource = new List<ETypeOfUcionice>() { ETypeOfUcionice.SA_RACUNARIMA, ETypeOfUcionice.BEZ_RACUNARA};

            if (ucionica.IdUcionice.Equals(0))
            {
                _status = Status.ADD;
                
            }
            else
            {
                _status = Status.EDIT;
            }
            selectedUcionica = ucionica;
            this.DataContext = ucionica;
        }

        private void BtnOK_Click(object sender, RoutedEventArgs e)
        {
            Ucionica ucionica = Data.DajUcionicuPoBrMesta(txtBrMesta.ToString());
            if (_status.Equals(Status.ADD))
            {
                if (ProveraUcionice(Convert.ToInt32(txtId.Text)))
                {
                    MessageBox.Show($"The classroom with id {txtId.Text}" +
                        $"already exists!", "Error", MessageBoxButton.OK);
                    return;
                }
                else
                {
                    if (cmbType.SelectedValue.ToString().Equals(ETypeOfUcionice.SA_RACUNARIMA.ToString()))
                    {
                        selectedUcionica.Ustanova.Ucionice.Add(selectedUcionica);
                        selectedUcionica.SaveUcionica();
                    }
                    else if(cmbType.SelectedValue.ToString().Equals(ETypeOfUcionice.BEZ_RACUNARA.ToString()))
                    {
                        if (ProveraPolja() == false)
                        {
                            return;
                        }

                        selectedUcionica.TypeOfUcionice = ETypeOfUcionice.BEZ_RACUNARA;
                        selectedUcionica.Ustanova.Ucionice.Add(selectedUcionica);
                        selectedUcionica.SaveUcionica();

                    }
                    
                    Data.Ucionice.Add(selectedUcionica);
                    selectedUcionica.SaveUcionica();
                }
            }
            if (_status.Equals(Status.EDIT))
            {
                ucionica = Data.GetUcionicaBrMestaEdit(txtBrMesta.Text, selectedUcionica);
                if (ucionica != null && _status.Equals(Status.EDIT) && ucionica.Active == true)
                {
                    MessageBox.Show($"Ucionica vec postoji", "Greska", MessageBoxButton.OK);
                    return;
                }
                if (ProveraPolja() == false)
                {
                    return;
                }
                selectedUcionica.UpdateUcionica();
            }

            this.DialogResult = true;
            this.Close();
        }

        public bool ProveraPolja()
        {
            if (txtBrMesta.Text.Equals(0) || txtBrUcionice.Equals(0))
            {
                MessageBox.Show($"Sva polja moraju biti popunjena!", "Greska", MessageBoxButton.OK);
                return false;
            }
            return true;
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private bool ProveraUcionice(int id)
        {
            foreach (Ucionica ucionica in selectedUcionica.Ustanova.Ucionice)
            {
                if (ucionica.IdUcionice.Equals(id))
                {
                    return true;
                }
            }
            return false;
        }

        private Ucionica GetUcionica(int id) 
        {
            foreach (Ucionica ucionica in Data.Ucionice)
            {
                if (ucionica.IdUcionice.Equals(id))
                {
                    return ucionica;
                }
            }
            return null;
        }
    }
}