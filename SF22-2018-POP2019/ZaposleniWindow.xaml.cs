﻿using SF22_2018_POP2019.Models;
using SF22_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF22_2018_POP2019
{
    /// <summary>
    /// Interaction logic for ZaposleniWindow.xaml
    /// </summary>
    public partial class ZaposleniWindow : Window
    {
        List<User> zaposleni = new List<User>();
        ICollectionView view;
        string selectedFilter = "Active";
        public ZaposleniWindow()
        {
            InitializeComponent();

            dgZaposleni.IsReadOnly = true;
            RefreshParametara();
            RefreshUstanove();
        }

        private bool CustomFilter(object obj)
        {
            User user = obj as User;
            return user.Active;
        }

        private void InitializeView()
        {
            view = CollectionViewSource.GetDefaultView(zaposleni);
            dgZaposleni.ItemsSource = view;
        }

        public List<User> DajZaposlene()
        {
            Console.WriteLine("Generisanje zaposlenih");
            List<User> zaposleni = new List<User>();
            if (cmbUstanove.SelectedItem != null)
            {
                Ustanova1 ustanova = Data.DajUstanovuPoNazivu(cmbUstanove.SelectedItem.ToString());
                foreach (Profesor pro in Data.Profesori)
                {
                    if (pro.Ustanova.IdUstanove == ustanova.IdUstanove)
                    {
                        zaposleni.Add(pro);
                        foreach (Asistent asi in pro.Assistants)
                        {
                            if (asi.Ustanova.IdUstanove == ustanova.IdUstanove)
                            {
                                zaposleni.Add(asi);
                            }
                        }
                    }
                }
                foreach (Asistent a in Data.Asistenti)
                {
                    if (a.Ustanova.IdUstanove == ustanova.IdUstanove)
                    {
                        zaposleni.Add(a);
                    }
                }
            }
            return zaposleni;
        }

        public void RefreshUstanove()
        {
            foreach (Ustanova1 u in Data.Ustanove)
            {
                cmbUstanove.Items.Add(u.Naziv);
            }
        }

        private void dgZaposleni_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Lozinka") || e.PropertyName.Equals("Ustanova") || e.PropertyName.Equals("Termini") || e.PropertyName.Equals("Error") || e.PropertyName.Equals("Active") || e.PropertyName.Equals("Id"))
                e.Column.Visibility = Visibility.Collapsed;
        }

        private void cmbUstanove_DropDownClosed(object sender, EventArgs e)
        {
            zaposleni = DajZaposlene();
            InitializeView();
            view.Filter = CustomFilter;
        }

        public void RefreshParametara()
        {
            cmbKorisnici.Items.Add("imenu");
            cmbKorisnici.Items.Add("prezimenu");
            cmbKorisnici.Items.Add("email-u");
            cmbKorisnici.Items.Add("korisnickom imenu");
            cmbKorisnici.Items.Add("tipu korisnika");
            cmbKorisnici.SelectedIndex = 0;
        }

        private string DajParametar()
        {
            String Pretraga = "";
            if (cmbKorisnici.SelectedItem != null)
            {
                switch (cmbKorisnici.SelectedItem.ToString())
                {
                    case "imenu":
                        {
                            Pretraga = "Name";
                        }
                        break;
                    case "prezimenu":
                        {
                            Pretraga = "LastName";
                        }
                        break;
                    case "email-u":
                        {
                            Pretraga = "Email";
                        }
                        break;
                    case "korisnickom imenu":
                        {
                            Pretraga = "Username";
                        }
                        break;
                    case "tipu korisnika":
                        {
                            Pretraga = "TypeOfUser";
                        }
                        break;
                    default:
                        break;
                }
            }
            return Pretraga;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void TxtPretraga_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (cmbUstanove.SelectedItem != null)
            {
                Ustanova1 ustanova = Data.DajUstanovuPoNazivu(cmbUstanove.SelectedItem.ToString());
                //Data.PretragaKorisnikaZaPosebnuUstanovu(txtPretraga.Text, DajParametar(), zaposleni, ustanova);
                view.Refresh();
            }
        }
    }
}
