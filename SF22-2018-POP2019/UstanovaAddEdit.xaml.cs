﻿using SF22_2018_POP2019.Models;
using SF22_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF22_2018_POP2019
{
    /// <summary>
    /// Interaction logic for UstanovaAddEdit.xaml
    /// </summary>
    public partial class UstanovaAddEdit : Window
    {
        private enum Status { ADD, EDIT }

        private Status _status;
        private Ustanova1 selectedUstanova;
        public UstanovaAddEdit(Ustanova1 ustanova)
        {
            InitializeComponent();

            if (ustanova.Naziv.Equals(""))
            {
                _status = Status.ADD;
            }
            else
            {
                _status = Status.EDIT;
            }
            selectedUstanova = ustanova;
            this.DataContext = ustanova;
        }

        public bool ProveraPolja()
        {
            if (txtNaziv.Text.Equals("") || txtLokacija.Equals(""))
            {
                MessageBox.Show($"Sva polja moraju biti popunjena!", "Greska", MessageBoxButton.OK);
                return false;
            }
            return true;
        }

        private void BtnCancel_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnOK_Click_1(object sender, RoutedEventArgs e)
        {
            Ustanova1 ustanova = Data.DajUstanovuPoNazivu(txtNaziv.Text);
            if (ustanova != null && _status.Equals(Status.ADD) && ustanova.Active == true)
            {
                MessageBox.Show($"Ustanova sa ovim nazivom {ustanova.Naziv} vec postoji", "Greska", MessageBoxButton.OK);
                return;
            }
            if (_status.Equals(Status.ADD))
            {
                if (ProveraPolja() == false)
                {
                    return;
                }
                Data.Ustanove.Add(selectedUstanova);
                selectedUstanova.SaveUstanova();
            }
            if (_status.Equals(Status.EDIT))
            {
                ustanova = Data.GetUstanovaNazivEdit(txtNaziv.Text, selectedUstanova);
                if (ustanova != null && _status.Equals(Status.EDIT) && ustanova.Active == true)
                {
                    MessageBox.Show($"Ustanova sa ovim nazivom {ustanova.Naziv} vec postoji", "Greska", MessageBoxButton.OK);
                    return;
                }
                if (ProveraPolja() == false)
                {
                    return;
                }
                selectedUstanova.UpdateUstanova();
            }
            this.DialogResult = true;
            this.Close();
        }
    }
}
