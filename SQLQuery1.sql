﻿create table dbo.Users (Id int not null identity(1,1) primary key,Username varchar(50) not null, Firstname varchar(50) not null, TypeOfUser varchar(10) not null, Active bit not null, Email varchar(50) not null)

create table dbo.Administrators (Id int not null primary key, constraint fk_users_administrators foreign key (Id) references dbo.Users(Id))

create table dbo.Profesor (Id int not null primary key, constraint fk_users_profesor foreign key(Id) references dbo.Users(id))

create table dbo.TeacherAssistants(Id int not null primary key, Profesor_Id int not null, constraint fk_users_tas foreign key (Id) references dbo.Users(Id), constraint fk_professors_tas foreign key (Profesor_Id) references dbo.Profesor(id))


