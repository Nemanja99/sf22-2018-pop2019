﻿CREATE TABLE [dbo].[termin] (
    [IdTermin]     INT     NOT NULL identity(1,1) primary key,
    [Vreme]        VARCHAR (20) NOT NULL,
    [DaniUNedelji] VARCHAR (30) NOT NULL,
    [TipNastave]   VARCHAR (30) NOT NULL,
    [Korisnik]     INT NOT NULL,
    [Active] BIT NOT NULL, 
);

