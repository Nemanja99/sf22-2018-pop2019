﻿CREATE TABLE [dbo].[ucionica] (
    [IdUcionice]  INT     NOT NULL identity(1,1) primary key,
    [BrUcionice]  INT          NOT NULL,
    [BrMesta]     INT          NOT NULL,
    [tipUcionice] VARCHAR (30) NOT NULL,
    [Active]      BIT          NOT NULL,
    [IdUstanove]  INT	       NOT NULL,
	constraint fk_ucionica_ustanova foreign key (IdUstanove) references dbo.ustanova(IdUstanove)
);


