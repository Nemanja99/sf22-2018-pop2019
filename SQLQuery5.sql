﻿create table users (Id smallint not null identity(1,1) primary key , FirstName varchar(25) not null, LastName varchar(30) not null, Email varchar(30) not null, Username varchar(30) not null, Lozinka varchar(40) not null, TypeOfUser varchar(20) not null, Active bit not null);

create table dbo.Administrators (Id smallint not null primary key, constraint fk_users_administrators foreign key (Id) references dbo.users(Id));
create table dbo.Profesor (Id smallint not null primary key, constraint fk_users_profesor foreign key(Id) references dbo.users(id));
create table dbo.TeacherAssistants(Id smallint not null primary key, Profesor_Id smallint not null, constraint fk_users_tas foreign key (Id) references dbo.users(Id), constraint fk_professors_tas foreign key (Profesor_Id) references dbo.Profesor(id));

insert into dbo.Users (Firstname, LastName , Email, Username, Lozinka , TypeOfUser, Active) values('Pera', 'Peric' , 'pera@gmail.com','pera123', 'peraadmin' , 'ADMIN' , 1);
insert into dbo.Users (Firstname, LastName , Email, Username, Lozinka , TypeOfUser, Active) values('Zika', 'Zikic' , 'zika@gmail.com','zika1234', 'zikacar' , 'ASISTENT' , 1);
insert into dbo.Users (Firstname, LastName , Email, Username, Lozinka , TypeOfUser, Active) values('Mika', 'Mikic' , 'mika@gmail.com','mikamika', 'mikamikic1' , 'PROFESOR' , 1);

insert into dbo.Administrators(Id) values(1);
insert into dbo.Profesor(Id) values(3);
insert into dbo.TeacherAssistants(Id, Profesor_Id) values(2, 3);

select * from dbo.Administrators
select * from dbo.Users 
select * from ustanova
