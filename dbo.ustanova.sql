﻿CREATE TABLE [dbo].[ustanova] (
    [IdUstanove] INT     NOT NULL identity(1,1) primary key,
    [Naziv]      VARCHAR (30) NOT NULL,
    [Lokacija]   VARCHAR (35) NOT NULL,
    [Active]     BIT          NOT NULL,
);

